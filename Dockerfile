# python image
FROM python:3.8

# allow logs of docker in console
ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1

# create folder in container
RUN mkdir /app

# assign current folder
WORKDIR /app

# copy local project in container
COPY . /app

# install requeriments of python on container
RUN python -m pip install -r requirements.txt
